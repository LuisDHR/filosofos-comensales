package com.ldhr.filosofos;

public class Cena 
{
	SemaforoBinario mutex = new SemaforoBinario(true);
	SemaforoContador[] sinc = new SemaforoContador[5];
	String[] estadof = new String[5];
	
	public Cena()
	{
		for (int i = 0; i < 5; i++){
			estadof[i] = "PENSAR";
			sinc[i] = new SemaforoContador(0);
		}
	}
	
	public void tomar_tenedores(int i)
	{
		mutex.P(); // Inicio de la CR
		
		estadof[i] = "HAMBRE";
		probar_bocado(i);
		
		mutex.V(); // Fin de la CR
		
		sinc[i].P(); // Si no pudo comer, espera
	}
	
	private void probar_bocado(int i)
	{
		int izq = (i-1)%5;
		int der = (i+1)%5;
		
		if(izq == -1)
			izq = 4;
		
		// Permitir COMER al filosofo [i], si está esperando. 
		if ( estadof[i] == "HAMBRE" 
			 && estadof[izq] != "COMER" 
			 && estadof[der] != "COMER") 
		{ 
			estadof[i] = "COMER"; 
			sinc[i].V(); 
		} 
		// Sino, simplemente no come pero sigue hambriento.
	}
	
	public void bajar_tenedores(int i) 
	{
		mutex.P(); // Iniciar la CR.
		           // Filosofo i:
		estadof[i] = "PENSAR";
		
		int izq = (i-1)%5;
		int der = (i+1)%5;
		
		if(izq == -1)
			izq = 4;
		
		probar_bocado( izq ); // Deja comer al filósofo de la izquierda, si es posible.
		
		probar_bocado( der ); // Deja comer al filósofo de la derecha, si es posible.
		
		mutex.V(); // se libera la CR.
	}

}
