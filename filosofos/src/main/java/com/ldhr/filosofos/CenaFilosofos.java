package com.ldhr.filosofos;

public class CenaFilosofos
{
    public static void main( String[] args )
    {
        // System.out.println( "Hello World!" );
        
    	Cena cena = new Cena();
        Filosofo[] filos = new Filosofo[5];
        
        for(int i = 0; i < filos.length; i++)
        {
        	filos[i] = new Filosofo(i, cena);
        }
        
        for(int i = 0; i < filos.length; i++)
        {
        	filos[i].start();
        }
    }
}
