package monitores;

import com.ldhr.filosofos.Util;

public class Filosofo extends Thread
{
	int i;
	Cena c = null;
	
	public Filosofo(int id, Cena cena)
	{
		i = id;
		c = cena;
		// this.start();
	}

	public void pensar()
	{
		System.out.println("Filósofo " + i + " pensando.");
		Util.mySleep(200);
	}
	
	public void comer()
	{
		System.out.println("Filósofo " + i + " comiendo.");
		Util.mySleep(200);
	}
	
	@Override
	public void run()
	{
		while (true)
		{
			pensar();
			c.tomar_tenedores(i);
			comer();
			c.bajar_tenedores(i);
		}
	}
}
