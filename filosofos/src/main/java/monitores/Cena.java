package monitores;

import com.ldhr.filosofos.Util;

public class Cena 
{
	String[] estadof = new String[5];
	
	// Variables de condición.
	int count = 0;
	// Número máximo de filósofos que pueden comer al mismo tiempo.
	final int maxFilosofos = 2;
	
	public Cena()
	{
		for (int i = 0; i < 5; i++){
			estadof[i] = "PENSAR";
		}
	}
	
	// Método de ingreso.
	public synchronized void tomar_tenedores(int i)
	{		
		estadof[i] = "HAMBRE";
		probar_bocado(i);
		
		// Condición de sincronización.
		while(count == maxFilosofos)
			Util.myWait( this );
		
		count++;
	}
	
	private void probar_bocado(int i)
	{
		int izq = (i-1)%5;
		int der = (i+1)%5;
		
		if(izq == -1)
			izq = 4;
		
		// Permitir COMER al filósofo [i], si está esperando. 
		if ( estadof[i] == "HAMBRE" 
			 && estadof[izq] != "COMER" 
			 && estadof[der] != "COMER") 
		{ 
			estadof[i] = "COMER";
			// Operación de notificación: el filósofo i está comiendo.
			notify(); 
		} 
		// Sino, simplemente no come pero sigue hambriento.
	}
	
	// Método de ingreso.
	public synchronized void bajar_tenedores(int i)
	{
		estadof[i] = "PENSAR";
		
		int izq = (i-1)%5;
		int der = (i+1)%5;
		
		if(izq == -1)
			izq = 4;
		
		probar_bocado( izq ); // Deja comer al filósofo de la izquierda, si es posible.
		
		probar_bocado( der ); // Deja comer al filósofo de la derecha, si es posible.
		
		count--;
	}

}